const express = require("express");
const mongoose = require("mongoose");
const config = require("config");

const app = express();
const port = process.env.PORT || 5000;
const db = config.get("mongoURI");

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// connect db
mongoose
  .connect(db, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useCreateIndex: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("Connected to DB"))
  .catch((err) => console.log(err));

app.use("/api/users", require("./routes/api/users"));

app.listen(port, () => console.log(`Server running on port ${port}`));
