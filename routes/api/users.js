const express = require("express");
const bcrypt = require("bcryptjs");
const config = require("config");
const jwt = require("jsonwebtoken");

const router = express.Router();

const auth = require("../../middleware/auth");

const User = require("../../model/user");

const jwtSecret = config.get("JWTsecret");

// route: POST api/users/login
// desc: User Login
// access: public

router.post("/login", async (req, res) => {
  const { email, password } = req.body;

  // validation
  if (!email || !password)
    return res.status(400).json({ msg: "Please enter all fields." });

  try {
    const user = await User.findOne({ email });
    if (!user) throw Error(" That User does not exist. ");

    const isMatch = await bcrypt.compare(password, user.password);

    if (!isMatch) throw Error("Password does not match");

    const token = jwt.sign({ id: user._id }, jwtSecret, { expiresIn: 3600 });
    if (!token) throw Error("Couldn't Authenticate.");

    res.status(200).res.json({
      token,
      user: {
        id: user._id,
        name: user.name,
        email: user.email,
      },
    });
  } catch (err) {
    res.status(400).json({ msg: err.message });
  }
});

// route: POST api/users/register
// desc: User registration
// access: public
router.post("/register", async (req, res) => {
  const { name, email, password } = req.body;

  if (!name || !email || !password) {
    return res.status(400).json({ message: "Please enter all fields." });
  }

  try {
    const user = await User.findOne({ email });
    if (user) throw Error("User already exist, please use another email.");

    const salt = await bcrypt.genSalt(10);
    if (!salt) throw Error("There was an issue with storing your password.");

    const hash = await bcrypt.hash(password, salt);
    if (!hash) throw Error("Error with hashing password seturity.");

    const newUser = new User({
      name,
      email,
      password: hash,
    });

    const savedUser = await newUser.save();
    if (!savedUser) throw Error("Youe user profile was not saved.");

    const token = jwt.sign({ id: savedUser._id }, jwtSecret, {
      expiresIn: 3600,
    });
    if (!token) throw Error("Couldn't Authenticate.");

    res.status(200).json({
      token,
      user: {
        id: savedUser._id,
        name: savedUser.name,
        email: savedUser.email,
        password: savedUser.password,
      },
    });
  } catch (err) {
    res.status(400).json({ msg: err.message });
  }
});

// route: GET api/users/all
// desc: GET all users
// access: private

router.get("/", auth, async (req, res) => {
  try {
    const users = await User.find();
    if (!users) throw Error("No Users Exist");

    res.json(users);
  } catch (err) {
    res.status(400).json({
      msg: err.message,
    });
  }
});

module.exports = router;
